/// database is a container where we keep data // 
// mongodb is our database
// 
// mongo and mongod // mongod is for initializing connection

//connect to database
// mongo .....>>>>> 
// >
/// shell command////////

// show dbs // it will list all the availabel databases
// creating a database ///
// use <db_name> if db_name exist it will select existing database else create new database
// db view selected database
// list availabel collections
// show collections ==> list all the availabel collections
// creating collection
// db.<collection_name>.insert({valid json});
// this command will create collection as well as insert data into collection

//fetching from database
// db.<col_name>.find({}); // that object part will be query field
////count 
// db.<col_name>.find({}).count() // it will return total number of items
// to prettify
// use .pretty()
//update
// db.<collection_name>.update({},{$set:{data to be updated}},{flag haru set garchau}) // 1st objec is for query

//remove//
// db.coll_name.remove({})// 


// dropping collection
// db.col_name.drop(); // 
// dropping database
// db.dropDatabase()


// BACK UP and RESTORE

// back up can be done in two ways // bson structure arko json and csv
// 1 bson
// mongodump // this command will back up bson data into defult dump folder
// mongodump --db <db_name> selected database into dump folder
// mongodump --db <db_name> --out path_to_destination  selected database into desired folder

// restore
// mongorestore // this will take backup data from dump folder
// mongorestore path_to_bson_backup_folder

// json format//
// mongoexport --db <db_name> --collection<col_name> --out path_to_destination with .json file
// mongoexport -d <db_name> -c <col_name> -o path_to_destination with .json file

// restore json
// mongoimport --db <db_name> --collection <col_name> path_to_json file

// csv format
// mongoexport --db <db_name> --collection<col_name> --type=csv --fileds 'comma separeted headers' --out destination_path with csv extension

// csv import 
// mongoimport  --db <db_new> --collection <col_name> --type=csv path_to_csv_file  --headerline