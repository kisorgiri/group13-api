var mongoose = require('mongoose');
let dbUrl;
if (process.env.DB == 'mlab') {
    dbUrl = 'mongodb://group13_14:group13_14@ds249017.mlab.com:49017/group13_14'
} else {
    dbUrl = 'mongodb://localhost:27017/group13db';
}

mongoose.connect(dbUrl, function(err, done) {
    if (err) {
        console.log('database connection failed');
    } else {
        console.log('database connection success');
    }
});