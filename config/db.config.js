var mongodb = require('mongodb');
var mongoClient = mongodb.MongoClient;
var conxnUrl = 'mongodb://localhost:27017';
var dbName = 'group13db';

module.exports = {
    client: mongoClient,
    url: conxnUrl,
    dbName: dbName
}