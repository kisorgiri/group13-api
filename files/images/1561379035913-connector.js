class vziConnectorSettings {
    description = '';
    isExpanded = false;
    isActive = false;
    callBackUrl = '';

    constructor($translate, $window, Common, ConnectorService, Connectors, MessageService, MessageTypes, SweetAlert) {
        'ngInject';

        this.$translate = $translate;
        this.$window = $window;
        this.Common = Common;
        this.ConnectorService = ConnectorService;
        this.Connectors = Connectors;
        this.MessageService = MessageService;
        this.MessageTypes = MessageTypes;
        this.SweetAlert = SweetAlert;
    }

    $onInit() {
        this.initConnector();
        this.unregisterWindowListener = this.MessageService.subscribeToWindowMessageEvent((message) => {
            if (message.data === this.MessageTypes.quickBooksConnect) {
                this.onConnected && this.onConnected({
                    connectorType: this.Connectors.QuickBooks,
                    connector: this.connector,
                });
                this.SweetAlert.swal({
                    title: this.$translate.instant('component.vzi-connector-settings.quickbooks.connect.success'),
                    type: 'success',
                    showConfirmButton: false,
                    timer: this.Common.sweetAlert.autoCloseTimeoutMs,
                });
            }
        });
    }

    $onChanges(changesObject) {
        this.initConnector();
    }

    $onDestroy() {
        this.unregisterWindowListener && this.unregisterWindowListener();
    }

    /**
     * Initializes connector related bindings
     */
    initConnector() {
        this.connectorName = this.connector.toLowerCase();
        this.isActive = this.connector && this.connector.enabled && this.connector.connected;
        this.description = this._getConnectorDescription();

        this.ConnectorService.getcallBackUrl()
            .then(response => this.callBackUrl = response.data);
    }

    /**
     * Opens modal window to connect to QuickBooks
     */
    connect() {
        const popupWidth = 800;
        const popupHeight = 650;
        this.$window.open(
            this.callBackUrl,
            'ippPopupWindow',
            `location=1,width=${popupWidth},height=${popupHeight},left=${(this.$window.screen.width / 2) - (popupWidth / 2)},top=${(this.$window.screen.height / 2) - (popupHeight / 2)}`
        );
    }

    /**
     * Handles click on disconnect button
     */
    disconnect() {
        this.onDisconnect && this.onDisconnect({
            connectorType: this.Connectors[this.connector],
            connector: this.connector,
        });
    }

    /**
     * Handles change in enabled toggle
     * @param {boolean} isEnabled
     */
    changeEnabledStatus(isEnabled) {
        this.onStatusChange && this.onStatusChange({
            connectorType: this.Connectors[this.connector],
            connector: this.connector,
            status: isEnabled,
        });
    }

    /**
     * Returns connector description text
     * @private
     * @returns {string}
     */
    _getConnectorDescription() {
        if (this.connector && this.connector.connected) {
            if (this.connector.enabled) {
                return this.$translate.instant(`component.vzi-connector-settings.${this.connectorName}.status.enabled`);
            }
            else {
                return this.$translate.instant('component.vzi-connector-settings.quickbooks.status.connected');
            }
        }

        return this.$translate.instant('component.vzi-connector-settings.quickbooks.status.disconnected');
    }

    /**
    * Resolve dynamically the url of template
    * @returns {string}
    */
    getTemplateUrl() {
        return this.templatePath;
    }
}

export const vziConnectorSettingsQuickBooks = {
    bindings: {
        connector: '<',
        isLoading: '<',
        templatePath: '<?',
        onConnected: '&',
        onDisconnect: '&',
        onStatusChange: '&',
    },
    controller: vziConnectorSettings,
    template: '<ng-include src="$ctrl.getTemplateUrl()"/>',

};
