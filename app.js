var express = require('express');
var app = express(); // entire express framework is here in app
var morgan = require('morgan');
var bodyParser = require('body-parser');
var cors = require('cors');
var port = 9090;

//events ////

var events = require('events');
var event1 = new events.EventEmitter();
var event2 = new events.EventEmitter();

// trigger part  (emit method is used to trigger)
// listen part (on method is used to listen the event)
// listening part
event1.on('ram', function (data) {
    console.log('i am ram event listening >>', data);
});
event2.on('product', function (data) {
    console.log('data in product >>>', data);
})

// setTimeout(function () {
//     event1.emit('ram', 'hello ');
// }, 3000);

var socket = require('socket.io');
var io = socket(app.listen(9091));
var users = [];
io.on('connection', function (client) {
    var id = client.id;
    console.log('socket client connected to server');
    client.on('user', function (data) {
        users.push({
            id: id,
            name: data
        });
        client.emit('users', users);
        client.broadcast.emit('users', users);
    })
    client.on('new-msg', function (data) {
        console.log('data is >>>', data);
        client.emit('msg-return', data); // only for own client
        client.broadcast.to(data.receiver).emit('msg-reply', data); // brodcast for all other connected client
    });
    client.on('is-typing', function (data) {
        client.broadcast.to(data.receiver).emit('is-typing'); // brodcast for all other connected client

    })
    client.on('typing-stop', function (data) {
        client.broadcast.to(data.receiver).emit('typing-stop'); // brodcast for all other connected client

    })

    client.on('disconnect', function () {
        console.log('client disconnected');
        users.forEach(function (user, i) {
            if (user.id === id) {
                users.splice(i, 1);
            }
        });
        client.broadcast.emit('users', users);
    })

})

//events ////
// load router level middleware
var authRouter = require('./controllers/auth.routes');
var userRouter = require('./controllers/user.route');
var productRouter = require('./controllers/product.route')(event1);
var path = require('path');
require('./config/db');
//template engine setup
var pug = require('pug');
app.set('view engine', pug);
app.set('views', 'templates');

// app.set('ram', 'i am brodway');
// console.log(app.get('ram'));

//  load application level middleware
var authenticate = require('./middlewares/authenticate');
var authorize = require('./middlewares/authorize');
var checkTicket = require('./middlewares/checkTicket');
// third party middleware
app.use(morgan('dev'));
app.use(cors());

// inbuilt middleware to serve static file
// app.use(express.static('files')) locally within express application
app.use('/file', express.static(path.join(__dirname, 'files'))) // when other programme(client) request for file
app.use(bodyParser.urlencoded({
    extended: false
}));

app.use(express.json())
app.use('/auth', authRouter);
app.use('/user', authenticate, userRouter);
app.use('/product', authenticate, productRouter);
app.use('/notification', checkTicket, userRouter);
app.use('/messages', checkTicket, authenticate, authorize, userRouter);

app.use(function (req, res, next) {
    console.log('application level middleware');
    next({
        msg: 'Not Found',
        status: 404
    });
});
// error hanlding middleware
// error handling middleware is called with next with argument
app.use(function (err, req, res, next) {
    // tthis middleware with four argument is error handling middleware
    console.log('i am error handling middleware  middleware');
    console.log('what comes in >>>', err);
    res.status(err.status || 400);
    res.json({
        message: err.msg || err
    })
});



app.listen(port, function (err, done) {
    if (err) {
        console.log('failed');
    } else {
        console.log('server listening at port ' + port);
    }
});

// middlewares 
// middleware is a function that has access to http request object http response object
// middleware can modify http request object response object
// next middleware refrence
// middleware function must be place in between http request response cycle

// configuration block to place middleware
// app.use() app.use is configuration block for middlewares

//types of middleware
// 5 types of middleware
// 1 application level middleware
// 2 routing level middleware
// 3 inbuilt middleware
// 4 third party middleware
// 5 error handling middleware

// the order of middleware matters