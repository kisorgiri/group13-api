var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var userSchema = new Schema({
    // modeling
    name: String,
    address: {
        type: String,
        required: true
    },
    email: {
        type: String,
        unique: true,
        sparse: true
    },
    username: {
        type: String,
        required: true,
        unique: true,
        lowercase: true,
        trim: true
    },
    password: {
        type: String,
        required: true,
    },
    phoneNumber: {
        type: Number
    },
    gender: {
        type: String,
        enum: ['male', 'female', 'others']
    },
    status: {
        type: String,
        enum: ['active', 'inactive'],
        default: 'active'
    },
    role: {
        type: Number,
        enum: [1, 2, 3], // 1 for admin, 2 normal user , 3 visitors,
        default: 2
    },
    eula: {
        type: Boolean,
        default: false
    },
    dob: Date,
    updatedBy: String,
    passwordResetExpiry: Date,
}, {
    timestamps: true
});

var userModel = mongoose.model('user', userSchema)
module.exports = userModel;