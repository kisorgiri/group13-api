var express = require('express');
var router = express.Router();
var UserModel = require('./../models/user.model');
var passwordHash = require('password-hash');
var authorize = require('./../middlewares/authorize');
router.get('/', function (req, res, next) {
    console.log('req.loggedinuser >>>', req.loggedInUser);
    //fetch all data form databse and responsd to user
    UserModel.find({}
        // , {
        //     username: 1,
        //     _id: 0
        // }
    )
        .sort({
            _id: -1
        })
        // .limit(2)
        // .skip(1)
        .exec(function (err, users) {
            if (err) {
                return next(err);
            }
            res.json(users);
        })

});
// GET BY id
router.get('/:id', function (req, res, next) {
    console.log('req.loggedinuser >>>', req.loggedInUser);

    var id = req.params.id;
    console.log('id is >>>', id);
    // UserModel.find({ _id: id }, function (err, user) {
    //     if (err) {
    //         return next(err);
    //     }
    //     res.json(user);
    // })
    UserModel.findById(id, function (err, user) {
        if (err) {
            return next(err);
        }
        res.json(user)
    })
});

router.put('/:id', function (req, res, next) {
    console.log('req.loggedinuser >>>', req.loggedInUser);

    var id = req.params.id;
    console.log('req.body.ois >>', req.body);

    UserModel.findById(id).exec(function (err, user) {
        if (err) {
            return next(err);
        }
        if (user) {
            if (req.body.firstName)
                user.name = req.body.firstName;
            if (req.body.address)
                user.address = req.body.address;
            if (req.body.emailAddress)
                user.email = req.body.emailAddress;
            if (req.body.username)
                user.username = req.body.username;
            if (req.body.password)
                user.password = passwordHash.generate(req.body.password);
            if (req.body.role)
                user.role = req.body.role;
            if (req.body.date_of_birth)
                user.dob = req.body.date_of_birth;
            if (req.body.gender)
                user.gender = req.body.gender;
            if (req.body.phone)
                user.phoneNumber = req.body.phone;
            if (req.body.eula)
                user.eula = req.body.eula
            if (req.body.status)
                user.status = req.body.status
            user.updatedBy = req.loggedInUser.username;

            // console.log('user is now >>>', user);
            user.save(function (err, updated) {
                if (err) {
                    return next(err);
                }
                res.json(updated);
            })
        }
        else {
            next({
                msg: 'User not found'
            })
        }
    })
});

router.delete('/:id', authorize, function (req, res, next) {
    console.log('req.loggedinuser >>>', req.loggedInUser);

    var id = req.params.id;
    UserModel.findByIdAndRemove(id, function (err, user) {
        if (err) {
            return next(err);
        }
        if (user) {
            user.remove(function (err, removed) {
                if (err) {
                    return next(err);
                }
                res.json(removed);
            })
        } else {
            next({
                msg: 'User not found'
            })
        }
    })
})

router.post('/hello', function (req, res, next) {
    res.send('hello from user');
    console.log('here at post ');
    //fetch all data form databse and responsd to user
})

module.exports = router;


// benefits of using ODM (mongoose)
// schema based solution
// different Data Type then JS
// method
// middleware
// indexing is lot more easier