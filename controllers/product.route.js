var ProductModel = require('./../models/product.model');
var fs = require('fs');
var path = require('path');
var router = require('express').Router();
var multer = require('multer');
//quick demo for uploading
// var upload = multer({ dest: './uploads/' });
// diskstorage
var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, './files/images');
    },
    filename: function (req, file, cb) {
        cb(null, Date.now() + '-' + file.originalname);
    }
});

function imageFilter(req, file, cb) {
    var imageType = file.mimetype.split('/')[0];
    if (imageType == 'image') {
        cb(null, true);
    } else {
        req.fileError = true;
        cb(null, false);
    }
}
var upload = multer({
    storage: storage,
    fileFilter: imageFilter
});


function map_product_req(obj1, obj2) {
    if (obj2.name)
        obj1.name = obj2.name;
    if (obj2.category)
        obj1.category = obj2.category;
    if (obj2.brand)
        obj1.brand = obj2.brand;
    if (obj2.description)
        obj1.description = obj2.description;
    if (obj2.price)
        obj1.price = obj2.price;
    if (obj2.color)
        obj1.color = obj2.color;
    if (obj2.size)
        obj1.size = obj2.size;
    if (obj2.weight)
        obj1.details.weight = obj2.weight;
    if (obj2.body)
        obj1.details.body = obj2.body;
    if (obj2.origin)
        obj1.origin = obj2.origin;
    if (obj2.manuDate)
        obj1.manuDate = new Date(obj2.manuDate);
    if (obj2.expiryDate)
        obj1.expiryDate = new Date(obj2.expiryDate);
    if (obj2.warrentyStatus)
        obj1.warrentyStatus = obj2.warrentyStatus;
    if (obj2.warrentyPeroid)
        obj1.warrentyPeroid = obj2.warrentyPeroid;
    if (obj2.tags)
        obj1.tags = obj2.tags.split(',');

    return obj1;

}

module.exports = function (ev) {


    router.route('/')
        .get(function (req, res, next) {
            ProductModel.find({})
                .populate('user', {
                    username: 1
                })
                .sort({
                    _id: -1
                })
                .exec(function (err, products) {
                    if (err) {
                        return next(err);
                    }
                    ev.emit('product', products);
                    res.json(products);
                })
        })
        .post(upload.single('img'), function (req, res, next) {
            // file upload vai sake pachi
            console.log('file is <<<<>>>>', req.file);
            console.log('body is >>', req.body);
            // if (req.file) {
            //     var mimetype = req.file.mimetype;
            //     var imageType = mimetype.split('/')[0];
            //     console.log('mime type is >>>', imageType)
            //     if (imageType !== 'image') {
            //         fs.unlink(path.join(process.cwd(), 'files/images/' + req.file.filename), function (err, done) {
            //             if (err) {
            //                 console.log('file removing failed', err);
            //             } else {
            //                 console.log('file removed');
            //             }
            //         })
            //         return next({
            //             msg: 'Invalid File Format'
            //         })
            //     }
            // }

            if (req.fileError) {
                return next({
                    msg: 'Invalid file filters'
                })
            }
            var newProduct = new ProductModel({});
            var newMappedProduct = map_product_req(newProduct, req.body);

            newMappedProduct.user = req.loggedInUser._id;
            if (req.file) {
                newMappedProduct.image = req.file.filename;
            }
            newMappedProduct.save(function (err, done) {
                if (err) {
                    return next(err);
                }
                res.json(done);
            });
        });

    router.route('/search')
        .get(function (req, res, next) {
            var condition = {};
            var searchCondition = map_product_req(condition, req.query);
            console.log('get for search >>', searchCondition);

            ProductModel.find(searchCondition)
                .exec(function (err, products) {
                    if (err) {
                        return next(err);
                    }
                    res.json(products);
                })
        })
        .post(function (req, res, next) {
            console.log('req.body. is >>', req.body);
            var condition = {};
            var searchCondition = map_product_req(condition, req.body);
            if (req.body.minPrice) {
                searchCondition.price = {
                    $gte: req.body.minPrice
                }
            }
            if (req.body.maxPrice) {
                searchCondition.price = {
                    $lte: req.body.maxPrice
                }
            }
            if (req.body.minPrice && req.body.maxPrice) {
                searchCondition.price = {
                    $gte: req.body.minPrice,
                    $lte: req.body.maxPrice
                }
            }

            if (req.body.fromDate && req.body.toDate) {
                var fromDate = new Date(req.body.fromDate).setHours(0, 0, 0, 0);
                var toDate = new Date(req.body.toDate).setHours(23, 59, 59, 59);
                searchCondition.createdAt = {
                    $gte: new Date(fromDate),
                    $lte: new Date(toDate)
                }
            }
            console.log('search condition >>', searchCondition)

            ProductModel.find(searchCondition)
                .exec(function (err, products) {
                    if (err) {
                        return next(err);
                    }
                    res.json(products);
                })
        })

    router.route('/:id')
        .get(function (req, res, next) {
            console.log('i should not be error')
            ProductModel.findOne({
                _id: req.params.id
            })
                .then(function (data) {
                    res.json(data);
                })
                .catch(function (err) {
                    next(err);
                });
        })
        .put(upload.single('img'), function (req, res, next) {

            if (req.fileError) {
                return next({
                    msg: 'invalid file format'
                });
            }
            var id = req.params.id;
            ProductModel.findById(id)
                .exec(function (err, product) {
                    if (err) {
                        return next(err);
                    }
                    if (product) {
                        var oldImage = product.image;
                        var updatedMapProduct = map_product_req(product, req.body);
                        updatedMapProduct.user = req.loggedInUser._id;
                        if (req.file) {
                            updatedMapProduct.image = req.file.filename;
                        }
                        updatedMapProduct.save(function (err, updated) {
                            if (err) {
                                return next(err);
                            }
                            if (req.file) {
                                fs.unlink(path.join(process.cwd(), 'files/images/' + oldImage));
                            }
                            res.json(updated);
                        })
                    } else {
                        next({
                            msg: 'Product not found'
                        })
                    }
                })
        })
        .delete(function (req, res, next) {

            ProductModel.findById(req.params.id)

                .then(function (product) {
                    if (product) {
                        product.remove(function (err, removed) {
                            if (err) {
                                return next(err);
                            }
                            res.json(removed);
                        });
                    } else {
                        next({
                            msg: 'Product not found'
                        });
                    }
                })
                .catch(function (err) {
                    next(err);
                });

        });

    return router;
}