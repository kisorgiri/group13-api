var express = require('express');
var router = express.Router();
var UserModel = require('./../models/user.model');
var passwordHash = require('password-hash');
var count = 0;
var jwt = require('jsonwebtoken');
var config = require('./../config/index');
var sender = require('./../config/nodemailer.config.js');

function prepareMail(details) {
    var msgBody = {
        from: 'Smart Project Mgmt<noreply@abcd.com>',
        to: details.email + ',bhattarairaju78@gmail.com,ujjwalrisal84@gmail.com,rupesh.giree@gmail.com,sauravhshah99@gmail.com,yojen.shakya@gmail.com',
        replyTo: 'jskisor@gmail.com',
        subject: 'Forgot Password ✔',
        text: 'Forgot Password',
        html: `<p>Hi <strong>${details.name}</strong></p>
        <p>We noticed that you are having trouble logging into our system, please use the link below to reset your password</p>
        <p><a href="${details.link}" target="_blank">reset password</a></p>
        <p>If you have not requested to reset your password,kindly ignore this email.</p>
        <p>Regards,</p>
        <p>Smart Project Mgmt</p>`
    }
    return msgBody;
}
router.get('/login', function(req, res, next) {
    res.render('login.pug', {
        title: req.query.title || 'js',
        msg: req.query.msg || 'welcome to express'
    })
})
router.post('/login', function(req, res, next) {
    console.log('post request here ', req.body);
    // database stuff
    UserModel.findOne({
            $or: [{
                username: req.body.username,
            }, {
                email: req.body.username
            }]
        })
        .exec(function(err, user) {
            if (err) {
                return next(err);
            }
            // console.log('user >>>', user);

            if (user) {
                if (user.status == 'active') {
                    // password verification
                    var isMatched = passwordHash.verify(req.body.password, user.password);
                    if (isMatched) {
                        // geenerate token
                        var token = jwt.sign({
                                id: user._id
                            },
                            config.jwtSecret);

                        res.json({
                            token: token,
                            user: user
                        });
                    } else {
                        console.log('count is >>', count);
                        if (count > 3) {
                            // update user with status inactive
                            // provide appropriate information
                            console.log('disable user');
                        }
                        count++;
                        next({
                            msg: 'Password didnot matched'
                        })
                    }

                } else {
                    next({
                        msg: 'Your account is disabled please contact your system administrator'
                    })
                }

            } else {
                next({
                    msg: 'username didnot match'
                });
            }
        })

})
router.get('/register', function(req, res, next) {
    res.render('register.pug');
})
router.post('/register', function(req, res, next) {
    console.log('post request for register', req.body);
    // assume hamile db store garne kaam sakiyo
    // res.redirect('/auth/login');
    var newUser = new UserModel({});

    if (req.body.firstName)
        newUser.name = req.body.firstName;
    if (req.body.address)
        newUser.address = req.body.address;
    if (req.body.email)
        newUser.email = req.body.email;
    if (req.body.username)
        newUser.username = req.body.username;
    if (req.body.password)
        newUser.password = passwordHash.generate(req.body.password);
    if (req.body.role)
        newUser.role = req.body.role;
    if (req.body.date_of_birth)
        newUser.dob = req.body.date_of_birth;
    if (req.body.gender)
        newUser.gender = req.body.gender;
    if (req.body.phone)
        newUser.phoneNumber = req.body.phone;
    if (req.body.eula)
        newUser.eula = req.body.eula
    if (req.body.status)
        newUser.status = req.body.status
        // newUser.save(function (err, user) {
        //     if (err) {
        //         return next(err);
        //     }
        //     res.json(user);
        // })
    newUser.save()
        .then(function(data) {
            res.json(data);
        })
        .catch(function(err) {
            next(err);
        });

});

router.post('/forgot-password', function(req, res, next) {
    UserModel.findOne({
            email: req.body.email
        })
        .exec(function(err, user) {
            if (err) {
                return next(err);
            }
            if (user) {

                var resetLink = req.headers.origin + '/auth/reset-password/' + user.username;
                var mailBody = prepareMail({
                    name: user.username,
                    email: req.body.email,
                    link: resetLink
                });
                console.log('mailbody >>', mailBody);
                user.passwordResetExpiry = new Date().getTime() + 1000 * 60 * 60 * 24 * 2;
                // user.password = null;
                user.save(function(err, saved) {
                    if (err) {
                        return next(err);
                    }
                    sender.sendMail(mailBody, function(err, done) {
                        if (err) {
                            return next(err);
                        } else {
                            res.json(done);
                        }
                    })
                })


            } else {
                next({
                    msg: 'User not registered with provided email'
                })
            }
        })
});

router.post('/reset-password/:username', function(req, res, next) {
    var username = req.params.username;
    UserModel.findOne({
            username: username
        })
        .exec(function(err, user) {
            if (err) {
                return next(err);
            }
            if (user) {
                var resetTime = new Date(user.passwordResetExpiry).getTime();
                var now = Date.now();
                if (now > resetTime) {
                    return next({
                        msg: "password reset link expired"
                    });
                }
                user.password = passwordHash.generate(req.body.password);
                user.passwordResetExpiry = null;
                user.save(function(err, done) {
                    if (err) {
                        return next(err);
                    }
                    res.json(done);
                })
            } else {
                next({
                    msg: 'User not found'
                })
            }
        })
})


module.exports = router;